module.exports = function(grunt) {
    var manifest = JSON.parse(grunt.file.read('manifest.webapp'));


    grunt.initConfig({
        watch: {
            index: {
                files: [ 'src/index.html','manifest.webapp'],
                tasks: ['template'],
                options: {
                    spawn: false,
                },
            },
            scripts: {
                files: [ 'src/css/*.css'],
                tasks: ['copy'],
                options: {
                    spawn: false,
                },
            },
        },
        template: {
            'options': {},
            'index':{
                'options': {
                    'data': manifest
                },
                'files':{
                    'index.html': [ 'src/index.html']
                }
            }
        },

        jshint: {
            all: ['Gruntfile.js', 'js/**/*.js']
        },
        copy:
        {
            main:
            {
                files: [
                     {cwd: 'src/css', expand: true, src: ['**'], dest: 'css/', filter: 'isFile'}
                ]
            }
        },
        htmlhint: {
          prod: {
            options: {
              'tag-pair': true
            },
            src: ['index.html']
          }
        }
    });

    grunt.loadNpmTasks('grunt-template');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-htmlhint');

    grunt.registerTask('build', ['template','copy']);
    grunt.registerTask('default', ['template','copy']);
};

