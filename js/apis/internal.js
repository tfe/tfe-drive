var Internal = function(){};
Internal.prototype = new Apiprot();

Internal.prototype.icon='cloud';
Internal.prototype.type='Internal';
Internal.prototype.dbname='Internal';
Internal.prototype.star_available = false;
Internal.prototype.delete_available = false;
Internal.prototype.download_available=false;
//Internal.prototype.move_folder_available=false;
Internal.prototype.upload_available=false;
Internal.prototype.nocache=true;
Internal.prototype.copy_folder=false;

Internal.prototype.init = function()
{
    var self=this;
    this.accounts = {};

    sdcard.sdcards.forEach(function(sub)
    {
        var account=
        {
            id: 'internal_'+sub.storageName,
            email: sub.storageName || translate('internal_sdcards'),
            sdcard: sub,
            type: 'sdcard'
        };
        accounts.add(account, self);
    });

    this.initDb();
};

Internal.prototype.setAccount = function(id)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.sdcards.forEach(function(sub)
        {
            var account=
            {
                id: 'internal_'+sub.storageName,
                email: sub.storageName || translate('internal_sdcards'),
                sdcard: sub,
                type: 'sdcard'
            };
            if(account.id==id)
            {
                self.account =  account;
                return ok();
            }
        });
        reject();
    });
};


Internal.prototype.list_dir = function(id)
{
    var self=this;

    var result = [];

    return new Promise(function(ok, reject)
    {
        id=id.replace(self.account.id+'_','');
        if(id=='root' && sdcard.sdcards.length>1)
        {
            sdcard.sdcards.forEach(function(subsdcard)
            {
                var subid = self.account.sdcard.storageName+'/';
                result.push(
                {
                    parent: self.account.id+'_'+subid,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+subid,
                    realid: id,
                    name: subsdcard.storageName || translate('internal_sdcards'),
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });
            return ok({items:result});
        }
        else if(id=='/' || id=='root')
        {
            id = self.account.sdcard.storageName+'/';
        }
        sdcard.list_dirs().then(function(dirs)
        {
            var segments = id.split(/\//);
            var found=true;
            segments.forEach(function(segment)
            {
                if(segment)
                {
                    if(dirs.dirs[segment])
                    {
                        dirs = dirs.dirs[segment];
                    }
                    else
                    {
                        console.error('cannot find segment ',segment);
                        return ok({items:[]});
                    }
                }
            });

            Object.keys(dirs.dirs).forEach(function(dir)
            {
                result.push(
                {
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+id+dir+'/',
                    realid: id+dir+'/',
                    name: dir,
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });

            sdcard.list_files(id,true).then(function(files)
            {
                files.forEach(function(file)
                {
                    var filename = file.name.replace(/^.*\//,'');
                    var mime =filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'') : '???';
                    var icon =  'page_white';

                    mimeData = self.getMimeAndIcon(filename);
                    mime = mimeData.mime;
                    icon=mimeData.icon;
                    var is_image = /image/.test(mime);

                    result.push({
                        parent: self.account.id+'_'+id,
                        icon: 'img/icons/dropbox/'+(icon)+'.gif',
                        mime: mime,
                        creation_date: null,
                        modified_date: file.lastModifiedDate.getTime(),
                        id: self.account.id+'_'+id+filename,
                        realid: id+'/'+filename,
                        size: file.size,
                        name: filename,
                        thumbnail: is_image ? 'sdcard://'+id+filename+'@400x400' : null,
                        bigthumbnail: is_image ? 'sdcard://'+id+filename : null,
                        download: id+'/'+filename,
                        is_folder:  false,
                        is_starred:  false,
                        is_hidden:  false,
                        is_trashed:  false
                    });
                });
                
                ok({items:result});
            }, function()
            {
                console.error('error list files');
                reject();
            });
        }, reject);
    });
};


Internal.prototype.open = function(received_item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.getfile(received_item.realid.replace(/^\//,'')).then(function(file)
        {
            //file.name=received_item.name;

            if(CustomActivity.isPicking)
            {
                CustomActivity.send(file);
                return ok();
            }

            var activity = new Activity({
                name: 'open',
                data: {
                type: file.type,
                allowSave: true,
                blob: file,
                title: received_item.name,
                name: file.name
                }
            });
            activity.onsuccess= function(r)
            {
                ok();
            };
            activity.onerror = function() {
                console.error('error ',this.error);
                if(this.error.name==='NO_PROVIDER')
                {
                    files.no_provider(file);
                    ok();
                }
                else
                {
                    console.error('big fail? ',this.error, received_item);
                    reject();
                }
            };
        }, function(err)
        {
            console.error('fail open ',err,this);
            reject();
        });
    });
};

Internal.prototype.rename = function(item, newname, is_folder)
{
    var id = item.id;
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = id.replace(/[^\/]+\/?$/, newname);
                sdcard.add(file, dest, true).then(
                function()
                {
                    sdcard.delete(id,true).then(ok, reject);
                }, reject);
            }, reject);
        }
        else
        {
            var dest = id.replace(/[^\/]+\/?$/, newname+'/');
            self.move(id, dest, true, { from: id, to: dest}).then(ok, reject);
        }
    });
};

Internal.prototype.delete = function(item, is_folder)
{
    var self=this;
    var id = item.id || item;
    console.log('deleting ',item);
    var id = item.id;
    
    id=id.replace(self.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        sdcard.delete(id,true).then(ok, reject);
    });
};

Internal.prototype.share = function(item)
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        sdcard.getfile(item.realid.replace(/^\//,'')).then(function(file)
        {
            return ok(file);
        }, function()
        {
            return reject(translate('error_opening_file'));
        });
    });
};

Internal.prototype.move = function(id, destination, is_folder, rename_data)
{
    // TODO
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');

    console.log('MOVE '+id+' to '+destination);
    if(id==destination)
    {
        return;
    }

    if(destination=='root' || destination=='/')
    {
        destination = self.account.sdcard.storageName+'/';
    }

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            sdcard.getfile(id).then(function(file)
            {
                dest = destination+'/'+id.replace(/.*\//, '');
                console.log('will move '+id+' to '+dest);
                sdcard.add(file, dest, true).then(function()
                    {
                        sdcard.delete(id,true).then(ok, reject);
                    }, reject);
                ok();
            }, reject);
        }
        else
        {
            var promises=[];
            self.list_dir(id).then(function(dirs)
            {
                var start_path = id.replace(/[^\/]+\/$/,'');
                console.log('start path ', id, start_path);
                prefix = id.replace(start_path,'');

                dirs.items.forEach(function(item)
                {
                    var dest;
                    dest = destination+prefix;
                    if(rename_data)
                    {
                        dest = item.id.replace(rename_data.from, rename_data.to).replace(/[^\/]+\/?$/,'');
                    }

                    console.log('will move dir ',item.id+' to '+dest, rename_data);
                    promises.push(self.move(item.id, dest, item.is_folder, rename_data));
                });
            }, reject)
            .then(function()
            {
                console.log('Promises ',promises.length);
                return Promise.all(promises).then(function()
                {
                    console.log('delete diR! '+id);
                    self.delete(id).then(ok, reject);
                }, function()
                {
                    console.error('error moving '+id);
                    reject();
                });
            });
        }
    });
};

Internal.prototype.copy = function(id, destination, is_folder)
{
    var self=this;
    id=id.replace(self.account.id+'_','');
    destination=destination.replace(self.account.id+'_','');
    if(destination=='root' || destination=='/')
    {
        destination = self.account.sdcard.storageName+'/';
    }

    return new Promise(function(ok, reject)
    {
        if(!is_folder)
        {
            var file = sdcard.getfile(id).then(function(file)
            {
                var name = file.name.replace(/^.*\//,'');
                sdcard.add(file,destination+'/'+name,true).then(ok, reject);
            },reject);
        }
    });
};

Internal.prototype.mkdir = function(id, newname)
{
    var self=this;
    id=id.replace(self.account.id+'_','');



    return new Promise(function(ok, reject)
    {
        var dir =
        {
            parent: self.account.id+'_'+id,
            icon: 'img/icons/dropbox/folder.gif',
            mime: '',
            creation_date: null,
            modified_date: null,
            id: self.account.id+'_'+id+newname+'/',
            realid: id+newname+'/',
            name: newname,
            thumbnail: null,
            size: 0,
            download: null,
            is_folder:  true,
            is_starred:  false,
            is_hidden:  false,
            is_trashed:  false
        };

        var blob = new Blob( [ '' ], { type: 'text/plain'} );
        blob.name='/.empty';
        self.create(dir.id+'/.empty', blob).then(function()
        {
            ok(dir);
        }, reject);
    });
}

Internal.prototype.create = function(id,blob)
{
    var self=this;
    id=id.replace(this.account.id+'_','');

    return new Promise(function(ok, reject)
    {
        // Add sdcard prefix to destination
        var dest= self.account.sdcard.storageName+'/'+id+blob.name.replace(/.*\//,'');
        sdcard.add(blob, dest, true).then(ok, reject);
    });
};


Internal.prototype.getInfo = function(account)
{
    return new Promise(function(ok, reject)
    {
        var sdcard_sub = account.sdcard;
        Promise.all([sdcard.getUsedSpace(sdcard_sub), sdcard.getFreeSpace(sdcard_sub)])
        .then(function(values)
        {
            ok((sdcard_sub.storageName || '')+' '+ files.getReadableFileSizeString(values[0])+' / '+ files.getReadableFileSizeString(values[1]+values[0]));
        }, function(e)
        {
            reject();
        });
    });
};

Internal.prototype.search = function(search)
{
    var self=this;

    return new Promise(function(ok, reject)
    {
        sdcard.search(search).then(function(data)
        {
            var result = [];

            data.dirs.forEach(function(dir)
            {
                id= dir.replace(/^(.*\/)[^\/]+\/$/,'$1');
                dirname= dir.replace(/^(.*\/)?([^\/]+)\/$/,'$2');

                result.push(
                {
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/folder.gif',
                    mime: '',
                    creation_date: null,
                    modified_date: null,
                    id: self.account.id+'_'+dir,
                    realid: dir,
                    name: dirname,
                    thumbnail: null,
                    size: 0,
                    download: null,
                    is_folder:  true,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });

            data.files.forEach(function(file)
            {
                var id=file.name;
                var filename = file.name.replace(/^.*\//,'');
                var mime =filename.indexOf('.')!==-1 ? filename.replace(/^.*\./,'') : '???';
                var icon =  'page_white';

                mimeData = self.getMimeAndIcon(filename);
                mime = mimeData.mime;
                icon=mimeData.icon;
                var is_image = /image/.test(mime);

                result.push({
                    parent: self.account.id+'_'+id,
                    icon: 'img/icons/dropbox/'+(icon)+'.gif',
                    mime: mime,
                    creation_date: null,
                    modified_date: file.lastModifiedDate.getTime(),
                    id: self.account.id+'_'+id,
                    realid: id,
                    size: file.size,
                    name: filename,
                    thumbnail: is_image ? 'sdcard://'+id+'@400x400' : null,
                    bigthumbnail: is_image ? 'sdcard://'+id : null,
                    download: id,
                    is_folder:  false,
                    is_starred:  false,
                    is_hidden:  false,
                    is_trashed:  false
                });
            });
            ok({items:result});
        }, function()
        {
            console.error('error calling sdcard.search');
            reject();
        });
    });
};
