var Settings = function()
{
    this.apis=[];
};

Settings.prototype.bind = function()
{
    this.settings = document.querySelector('.settings');
    this.show_labels = document.querySelector('#show_labels');
    this.show_thumbnails = document.querySelector('#show_thumbnails');
    this.show_details = document.querySelector('#show_details');
    this.click_vibrate = document.querySelector('#click_vibrate');
    this.use_gallery = document.querySelector('#use_gallery');
    this.enable_cache = document.querySelector('#enable_cache');
    this.view_notification = document.querySelector('#view_notification');
    this.enable_sync = document.querySelector('#enable_sync');
    this.recent_time = document.querySelector('#recent_time');
    this.invalid_recent_time = document.querySelector('#invalid_recent_time');
    this.touch_multiselect = document.querySelector('#touch_multiselect');
    this.invalid_touch_multiselect = document.querySelector('#invalid_touch_multiselect');
    this.cache_location = document.querySelector('#cache_location');
    this.sync_time = document.querySelector('#sync_time');
    this.clear_cache = document.querySelector('#clear_cache');
    this.open_changelog = document.querySelector('#open_changelog');

    this.toggle_buttons = document.querySelectorAll('.checkbox_settings'); 
};

Settings.prototype.init = function()
{
    var self=this;
    this.bind();

    // Add sdcards list
    sdcard.sdcards.forEach(function(sdcard_sub)
    {
        var option = document.createElement('option');
        option.value=sdcard_sub.storageName;
        option.innerHTML=sdcard_sub.storageName || translate('default_storage');
        self.cache_location.appendChild(option);
    });
    document.querySelector('#cache_location').addEventListener('change', function(e) { self.set_cachelocation(e.target.value, true); self.restoreSettings(); });
    document.querySelector('#sync_time').addEventListener('change', function(e) { self.set_synctime(e.target.value, true); self.restoreSettings(); });

    Array.forEach(document.querySelectorAll('.goto_settings'), function(item)
    {
        item.addEventListener('click', vibrate.button.bind(vibrate));
        item.addEventListener('click', self.open.bind(self));
    });

    // Toggle fieldset
    Array.forEach(document.querySelectorAll('fieldset.toggle legend'), function(item)
    {
        item.addEventListener('click', vibrate.button.bind(vibrate));
        item.addEventListener('click', self.toggle_legend.bind(self));
    });

    document.querySelector('#lang').addEventListener('change', function(e) { return self.set_lang(e.target.value, true); });

    Array.forEach(this.toggle_buttons, function(button)
    {
        button.addEventListener('click', vibrate.button.bind(vibrate));
        button.addEventListener('click',self.toggle_checkbox.bind(self,button));

        var id = button.getAttribute('data-id');
        id = id.charAt(0).toUpperCase()+id.substr(1);
    });

    this.clear_cache.addEventListener('click', vibrate.button.bind(vibrate));
    this.clear_cache.addEventListener('click', files.clearAllCache.bind(files));

    this.open_changelog.addEventListener('click', vibrate.button.bind(vibrate));
    this.open_changelog.addEventListener('click', this.openChangelog.bind(this));

    this.recent_time.addEventListener('change', function(e) { return self.updateRecentTime(e,1); });
    this.recent_time.addEventListener('keyup', function(e) { return self.updateRecentTime(e,0); });

    this.touch_multiselect.addEventListener('change', function(e) { return self.updateTouchMultiselect(e,1); });
    this.touch_multiselect.addEventListener('keyup', function(e) { return self.updateTouchMultiselect(e,0); });

    this.restoreSettings();
};


Settings.prototype.getShowDetails = function() { return this.get_checkbox_value(this.show_details); };
Settings.prototype.getShowThumbnails = function() { return this.get_checkbox_value(this.show_thumbnails); };
Settings.prototype.getShowLabels = function() { return this.get_checkbox_value(this.show_labels); };
Settings.prototype.getClickVibrate = function() { return this.get_checkbox_value(this.click_vibrate); };
Settings.prototype.getUseGallery = function() { return this.get_checkbox_value(this.use_gallery); };
Settings.prototype.getEnableSync = function() { return this.get_checkbox_value(this.enable_sync); };
Settings.prototype.getEnableCache = function() { return this.get_checkbox_value(this.enable_cache); };
Settings.prototype.getViewNotif = function() { return this.get_checkbox_value(this.view_notification); };


Settings.prototype.toggle_checkbox = function(item)
{
    var value = this.get_checkbox_value(item);
    localStorage.setItem(item.getAttribute('data-id'), !value);
    var value = this.get_checkbox_value(item);
    this.restoreSettings();
};

Settings.prototype.get_checkbox_value = function(item)
{
    var value = localStorage.getItem(item.getAttribute('data-id'));
    if(value===null || value===undefined)
    {
        value = item.getAttribute('data-default')==='true' ? 'true' : 'false';
    }
    return value === 'true' ? true : false;
};

Settings.prototype.toggle_legend = function(item)
{
    var fieldset = item.target;
    while(fieldset && fieldset.tagName!=='FIELDSET')
    {
        fieldset = fieldset.parentNode;
    }
    if(fieldset.classList.contains('opened'))
    {
        fieldset.classList.remove('opened');
    }
    else
    {
        fieldset.classList.add('opened');
    }
};

Settings.prototype.open = function()
{
    this.settings.classList.add('focused');
};
Settings.prototype.close = function()
{
    this.settings.classList.remove('focused');
};

Settings.prototype.set_synctime=function(value)
{
    var options=[];
    var select = document.querySelector('#sync_time');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(value+'');
    localStorage.setItem('syncTime', value);
};

Settings.prototype.set_cachelocation=function(value)
{
    var options=[];
    var select = document.querySelector('#cache_location');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(value+'');
    localStorage.setItem('cacheLocation', value);
};

Settings.prototype.set_lang=function(lang, reload)
{
    var options=[];
    var select = document.querySelector('#lang');
    Array.forEach(select.options, function(option)
    {
        options.push(option.value);
    });
    select.selectedIndex = options.indexOf(lang+'');

    if(reload)
    {
        localStorage.setItem('lang', lang);
        location.reload();
    }
};
Settings.prototype.getLang= function()
{
    return localStorage.getItem('lang') || navigator.language;
};

Settings.prototype.restoreSettings= function()
{
    var self=this;

    this.set_synctime(this.getSyncTime());
    this.set_cachelocation(this.getCacheLocation());
    this.set_lang(this.getLang());

    Array.forEach(this.toggle_buttons, function(button)
    {
        if(self.get_checkbox_value(button))
        {
            button.classList.remove('fa-toggle-off');
            button.classList.add('fa-toggle-on');
        }
        else
        {
            button.classList.add('fa-toggle-off');
            button.classList.remove('fa-toggle-on');
        }
    });

    if(this.get_checkbox_value(this.show_labels))
    {
        Array.forEach(document.querySelectorAll('.center_menu'), function(item)
        {
            item.classList.add('with_labels');
        });
    }
    else
    {
        Array.forEach(document.querySelectorAll('.center_menu'), function(item)
        {
            item.classList.remove('with_labels');
        });

    }

    if(this.get_checkbox_value(this.show_details))
    {
        files.files_container.classList.add('show_details');
    }
    else
    {
        files.files_container.classList.remove('show_details');

    }
    this.recent_time.value= this.getRecentTime();
    this.touch_multiselect.value= this.getTouchMultiselect();

    sdcard.update_settings();
    sync.init();
};


Settings.prototype.getSyncTime = function()
{
    var value= localStorage.getItem('syncTime');
    if(!value) { return 3600; } // default 3600 seconds =  1h
    return value;
};

Settings.prototype.getCacheLocation = function()
{
    var value= localStorage.getItem('cacheLocation');
    if(value===null) { return sdcard.sdcards[0].storageName; }
    return value; // default value, checked
};

Settings.prototype.updateRecentTime= function(e, do_warn)
{
    if(/^\d+$/.test(this.recent_time.value))
    {
        this.invalid_recent_time.classList.add('valid');
    }
    else
    {
        this.invalid_recent_time.classList.remove('valid');
        if(do_warn)
        {
           return files.alert(translate('invalid_recent_time'));
        }
    }

    if(do_warn)
    {
        localStorage.setItem('recentTime', this.recent_time.value);
        this.restoreSettings();
    }
};
Settings.prototype.updateTouchMultiselect= function(e, do_warn)
{
    if(/^\d+$/.test(this.touch_multiselect.value))
    {
        this.invalid_touch_multiselect.classList.add('valid');
    }
    else
    {
        this.invalid_touch_multiselect.classList.remove('valid');
        if(do_warn)
        {
           return files.alert(translate('invalid_touch_multiselect'));
        }
    }

    if(do_warn)
    {
        localStorage.setItem('touchMultiselect', this.touch_multiselect.value);
        this.restoreSettings();
    }
};
Settings.prototype.openChangelog = function()
{
    var self=this;
    return new Promise(function(ok, reject)
    {
        // Init XHR object
        var r = new XMLHttpRequest({ mozSystem: true });
        r.open('GET', 'Changelog.pm', true);
        r.setRequestHeader("ts",new Date());

        r.onreadystatechange = function () {
            if (r.readyState == 4)
            {
                if(r.status >= 200 && r.status< 400)
                {
                    // build selector
                    var changelog = r.responseText.split(/[\r\n]+/).splice(0,50);
                    var selector = new Selector();
                    selector.create(
                            '',
                            translate('changelog'),
                            changelog.join('<br>').replace(/#\s+([\d\.]+)/g,'<br><b>$1</b>'),
                            []);
                    ok();
                }
            }
        };
        r.send();
    });
};

Settings.prototype.getRecentTime = function()
{
    var value= localStorage.getItem('recentTime');
    if(value!==null) { return value; }
    return 7; // Default value
};
Settings.prototype.getTouchMultiselect = function()
{
    var value= localStorage.getItem('touchMultiselect');
    if(value!==null) { return value; }
    return 500; // Default value
};
